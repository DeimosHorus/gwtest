﻿using UnityEngine;
using UnityEngine.UI;

namespace MShtonda.SameGame.UI
{
    public class UISliderValue : MonoBehaviour
    {
        [SerializeField] private Text text;
        [SerializeField] private Slider slider;

        public int Value => Mathf.RoundToInt(slider.value);

        private void OnEnable()
        {
            slider.onValueChanged.AddListener(UpdateText);
            UpdateText(slider.value);
        }

        private void OnDisable()
        {
            slider.onValueChanged.RemoveListener(UpdateText);
        }

        private void UpdateText(float value) => text.text = Mathf.RoundToInt(value).ToString();
    }
}