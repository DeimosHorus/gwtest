﻿using UnityEngine;
using UnityEngine.UI;

namespace MShtonda.SameGame.UI
{
    public class NewGameWindow : MonoBehaviour
    {
        [SerializeField] private UISliderValue widthSlider;
        [SerializeField] private UISliderValue heightSlider;
        [SerializeField] private UISliderValue kindSlider;
        [SerializeField] private Toggle reverseToggle;
        [SerializeField] private Button startButton;
        
        public GameRouter Router { get; set; }

        private void OnEnable()
        {
            startButton.onClick.AddListener(OnStartClicked);
        }


        private void OnDisable()
        {
            startButton.onClick.RemoveListener(OnStartClicked);
        }
        
        private void OnStartClicked()
        {
            Router.StartGame(widthSlider.Value, heightSlider.Value, kindSlider.Value, reverseToggle.isOn);
        }
    }
}