﻿using UnityEngine;
using UnityEngine.UI;

namespace MShtonda.SameGame.UI
{
    public class FinishWindow : MonoBehaviour
    {
        [SerializeField] public Text header;
        [SerializeField] public Button newGameButton;
        
        public GameRouter Router { get; set; }

        private void OnEnable()
        {
            newGameButton.onClick.AddListener(OnNewGameClicked);
        }

        private void OnDisable()
        {
            newGameButton.onClick.RemoveListener(OnNewGameClicked);
        }

        public void SetHeader(bool isWon)
        {
            header.text = isWon ? "Победа!" : "Поражение!";
        }

        private void OnNewGameClicked()
        {
            Router.ShowNewGame();
        }
    }
}