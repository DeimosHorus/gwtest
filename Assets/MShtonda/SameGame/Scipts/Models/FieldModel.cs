﻿
namespace MShtonda.SameGame.Models
{
    public class FieldModel
    {
        private TileModel[,] _array;

        public int Width { get; set; }
        public int Height { get; }
        public int[] Heights { get; }

        public FieldModel(int width, int height)
        {
            _array = new TileModel[width, height];
            Heights = new int[width];
            for (int i = 0; i < width; i++)
            {
                Heights[i] = height;
            }

            Width = width;
            Height = height;
        }

        public TileModel this[int x, int y]
        {
            get => _array[x, y];
            set => _array[x, y] = value;
        }
    }
}