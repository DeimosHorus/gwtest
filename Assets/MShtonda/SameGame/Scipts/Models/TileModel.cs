﻿namespace MShtonda.SameGame.Models
{
    public class TileModel
    {
        public int X { get; set; }
        public int Y { get; set; }
        
        public short Type { get; }

        public bool IsDestroyed { get; set; } = false;

        public TileModel(int x, int y, short type)
        {
            X = x;
            Y = y;
            Type = type;
        }
    }
}