﻿using MShtonda.SameGame.Models;
using UnityEngine;

namespace MShtonda.SameGame.Views
{
    public class TileView : MonoBehaviour
    {
        private static readonly Color[] _colors =
        {
            Color.blue, Color.cyan, Color.green, Color.magenta, Color.red,
            Color.yellow, Color.white
        };

        [SerializeField] private SpriteRenderer image;

        public TileModel Model { get; private set; }

        public float Size => image.size.x;

        public bool IsSelected
        {
            set
            {
                var color = image.color;
                color.a = value ? 0.5f : 1f;
                image.color = color;
            }
        }

        public void Init(TileModel tileModel)
        {
            Model = tileModel;
            image.color = _colors[tileModel.Type];
        }
    }
}