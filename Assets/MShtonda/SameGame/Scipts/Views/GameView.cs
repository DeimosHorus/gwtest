﻿using System.Collections.Generic;
using Lean.Touch;
using MShtonda.SameGame.Models;
using MShtonda.SameGame.Views.Strategies;
using UnityEngine;

namespace MShtonda.SameGame.Views
{
    public class GameView : MonoBehaviour
    {
        [SerializeField] private TileView tilePrefab;
        
        [Range(0, 1)]
        [SerializeField] private float padding = 0.1f;
        
        private GameLogicService _gameLogicService;

        private TileView[,] _items;

        private List<TileView> _selected = new List<TileView>(20);

        private IPositionStrategy _positionStrategy;
        
        public GameRouter Router { get; set; }

        private void OnEnable()
        {
            LeanTouch.OnFingerTap += OnFingerTap;
        }
        
        private void OnDisable()
        {
            LeanTouch.OnFingerTap -= OnFingerTap;
        }

        private void OnFingerTap(LeanFinger finger)
        {
            var worldPosition = finger.GetWorldPosition(-Camera.main.transform.position.z);
            (int x, int y) = _positionStrategy.GetPosition(worldPosition);

            if (x < 0 || y < 0 || x >= _gameLogicService.FieldModel.Width || y >= _gameLogicService.FieldModel.Heights[x])
            {
                return;
            }

            if (_gameLogicService.IsSelected(x, y))
            {
                _gameLogicService.CollectSelected();
                _selected.Clear();
            }
            else
            {
                _gameLogicService.Select(x, y);
            }
        }


        public void NewGame(GameLogicService gameLogicService, bool reverseDirection)
        {
            Clear();
            
            _gameLogicService = gameLogicService;
            _gameLogicService.OnSelectedChanged += OnSelectedChanged;
            _gameLogicService.OnFieldUpdated += OnFieldUpdated;
            _gameLogicService.OnFinish += OnFinished;
            
            var fieldModel = _gameLogicService.FieldModel;
            var shift = tilePrefab.Size + padding;
            _positionStrategy = reverseDirection
                ? (IPositionStrategy)new ReversePositionStrategy(shift, fieldModel.Width, fieldModel.Height)
                : new DefaultPositionStrategy(shift, fieldModel.Width, fieldModel.Height);
            
            CreateTiles(fieldModel);
        }

        private void CreateTiles(FieldModel fieldModel)
        {
            _items = new TileView[fieldModel.Width, fieldModel.Height];
            
            for (int i = 0; i < fieldModel.Width; i++)
            {
                for (int j = 0; j < fieldModel.Heights[i]; j++)
                {
                    var instance = Instantiate(tilePrefab, transform);
                    instance.gameObject.name = $"tile{i}_{j}";
                    instance.transform.localPosition = _positionStrategy.GetWorldPosition(i, j);
                    instance.Init(fieldModel[i, j]);
                    _items[i, j] = instance;
                }
            }
        }

        private void Clear()
        {
            foreach (Transform child in transform)
            {
                Destroy(child.gameObject);
            }
        }

        private void OnFinished(bool isWin)
        {
            _gameLogicService.OnSelectedChanged -= OnSelectedChanged;
            _gameLogicService.OnFieldUpdated -= OnFieldUpdated;
            _gameLogicService.OnFinish -= OnFinished;
            Router.ShowFinish(isWin);
        }

        private void OnSelectedChanged(List<TileModel> selected)
        {
            foreach (var view in _selected)
            {
                view.IsSelected = false;
            }

            _selected.Clear();

            foreach (var item in selected)
            {
                var view = _items[item.X, item.Y];
                
                view.IsSelected = true;
                _selected.Add(view);
            }
        }

        private void OnFieldUpdated()
        {
            for (int i = 0; i < _items.GetLength(0); i++)
            {
                for (int j = 0; j < _items.GetLength(1); j++)
                {
                    var tile = _items[i, j];
                    if (ReferenceEquals(tile, null) || !tile.Model.IsDestroyed)
                    {
                        continue;
                    }
                    Destroy(tile.gameObject);
                    _items[i, j] = null;
                }
            }
            
            
            for (int i = 0; i < _items.GetLength(0); i++)
            {
                for (int j = 0; j < _items.GetLength(1); j++)
                {
                    var tile = _items[i, j];
                    if (ReferenceEquals(tile, null))
                    {
                        continue;
                    }

                    if (tile.Model.X != i || tile.Model.Y != j)
                    {
                        tile.transform.localPosition = _positionStrategy.GetWorldPosition(tile.Model.X, tile.Model.Y);
                        _items[i, j] = null;
                        _items[tile.Model.X, tile.Model.Y] = tile;
                    }
                }
            }
            
            _selected.Clear();
        }

    }
}