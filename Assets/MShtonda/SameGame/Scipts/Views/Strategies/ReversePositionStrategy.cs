﻿using UnityEngine;

namespace MShtonda.SameGame.Views.Strategies
{
    public class ReversePositionStrategy : IPositionStrategy
    {
        private float _shift;
        private Vector3 _start;
        private int _width;

        public ReversePositionStrategy(float shift, int width, int height)
        {
            _shift = shift;
            _start = new Vector3(_shift * (width - 1) * 0.5f, -_shift * (height - 1) * 0.5f);
            _width = width;
        }

        public Vector3 GetWorldPosition(int x, int y) => new Vector3(-_shift * x, _shift * y) + _start;

        public (int x, int y) GetPosition(Vector3 worldPosition)
        {
            var pos = worldPosition - _start;
            return (-Mathf.RoundToInt(pos.x / _shift), Mathf.RoundToInt(pos.y / _shift));
        }
    }
}