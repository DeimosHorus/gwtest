﻿using UnityEngine;

namespace MShtonda.SameGame.Views.Strategies
{
    public interface IPositionStrategy
    {
        public Vector3 GetWorldPosition(int x, int y);

        public (int x, int y) GetPosition(Vector3 worldPosition);
    }
}