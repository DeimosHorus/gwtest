﻿using System;
using System.Collections.Generic;
using System.Linq;
using MShtonda.SameGame.Models;
using Random = UnityEngine.Random;

namespace MShtonda.SameGame
{
    public class GameLogicService
    {
        private FieldModel _fieldModel;
        private List<TileModel> _selectedTiles;
        
        public event Action<List<TileModel>> OnSelectedChanged;
        public event Action OnFieldUpdated;
        public event Action<bool> OnFinish;

        public FieldModel FieldModel => _fieldModel;

        public void StartGame(int width, int height, int kinds, bool reverseDirection)
        {
            _fieldModel = new FieldModel(width, height);
            _selectedTiles = new List<TileModel>(20);

            for (var i = 0; i < width; i++)
            {
                for (var j = 0; j < height; j++)
                {
                    _fieldModel[i, j] = new TileModel(i, j, (short)Random.Range(0, kinds));
                }
            }
        }

        public bool IsSelected(int x, int y) => _selectedTiles.Any(t => t.X == x && t.Y == y);

        public bool IsValidTile(int x, int y) 
            => x >= 0 && x < _fieldModel.Width && y >= 0 && y < _fieldModel.Heights[x];

        public void Select(int x, int y)
        {
            _selectedTiles.Clear();
            var type = _fieldModel[x, y].Type;
            SelectRecursive(x, y, type);
            if (_selectedTiles.Count < 2)
            {
                _selectedTiles.Clear();
            }
            OnSelectedChanged?.Invoke(_selectedTiles);
        }

        public void CollectSelected()
        {
            if (_selectedTiles.Count < 0)
            {
                return;
            }

            foreach (var tile in _selectedTiles)
            {
                tile.IsDestroyed = true;
                _fieldModel[tile.X, tile.Y] = null;
            }

            var x = 0;
            for (int i = 0; i < _fieldModel.Width; i++)
            {
                var y = 0;
                for (int j = 0; j < _fieldModel.Heights[i]; j++)
                {
                    var tile = _fieldModel[i, j];
                    if (tile == null)
                    {
                        continue;
                    }

                    _fieldModel[x, y] = tile;
                    tile.X = x;
                    tile.Y = y;
                    y++;
                }

                if (y == 0)
                {
                    continue;
                }

                _fieldModel.Heights[x] = y;
                x++;
            }

            _fieldModel.Width = x;
            
            _selectedTiles.Clear();
            OnFieldUpdated?.Invoke();

            CheckFinishBattle();
        }

        private void SelectRecursive(int x, int y, short type)
        {
            if (!IsValidTile(x, y))
            {
                return;
            }
            var tile = _fieldModel[x, y];
            if (_selectedTiles.Contains(tile) || tile.Type != type)
            {
                return;
            }
            
            _selectedTiles.Add(tile);
            
            SelectRecursive(x - 1, y, type);
            SelectRecursive(x + 1, y, type);
            SelectRecursive(x, y - 1, type);
            SelectRecursive(x, y + 1, type);
        }

        private void CheckFinishBattle()
        {
            if (_fieldModel.Width == 0)
            {
                OnFinish?.Invoke(true);
                return;
            }
            
            for (int i = 0; i < _fieldModel.Width; i++)
            {
                for (int j = 0; j < _fieldModel.Heights[i]; j++)
                {
                    if (IsValidTile(i, j + 1) && _fieldModel[i, j].Type == _fieldModel[i, j + 1].Type
                        || IsValidTile(i + 1, j) && _fieldModel[i, j].Type == _fieldModel[i + 1, j].Type)
                    {
                        return;
                    }
                }
            }
            
            OnFinish?.Invoke(false);
        }
    }
}