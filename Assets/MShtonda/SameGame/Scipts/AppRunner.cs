﻿using UnityEngine;

namespace MShtonda.SameGame
{
    public class AppRunner : MonoBehaviour
    {
        [SerializeField] private GameRouter router;

        private GameLogicService _gameLogicService;

        private void Start()
        {
            _gameLogicService = new GameLogicService();
            router.GameLogicService = _gameLogicService;
            router.ShowNewGame();
        }
    }
}