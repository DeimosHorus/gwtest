﻿using MShtonda.SameGame.UI;
using MShtonda.SameGame.Views;
using UnityEngine;

namespace MShtonda.SameGame
{
    public class GameRouter : MonoBehaviour
    {
        [SerializeField] private NewGameWindow newGameWindow;
        [SerializeField] private FinishWindow finishWindow;
        [SerializeField] private GameView gameView;

        private GameObject _currentView;
        
        public GameLogicService GameLogicService { get; set; }

        public void ShowNewGame()
        {
            newGameWindow.Router = this;
            Show(newGameWindow);
        }

        public void ShowFinish(bool isWon)
        {
            finishWindow.Router = this;
            finishWindow.SetHeader(isWon);
            Show(finishWindow);
        }

        public void StartGame(int width, int height, int kinds, bool reverseDirection)
        {
            GameLogicService.StartGame(width, height, kinds, reverseDirection);
            gameView.Router = this;
            gameView.NewGame(GameLogicService, reverseDirection);
            Show(gameView);
        }

        private void Show(Component component) => Show(component.gameObject);

        private void Show(GameObject view)
        {
            CloseCurrent();
            _currentView = view;
            view.gameObject.SetActive(true);
        }

        private void CloseCurrent()
        {
            if (!ReferenceEquals(_currentView, null))
            {
                _currentView.gameObject.SetActive(false);
                _currentView = null;
            }
        }
    }
}